from datetime import datetime

class pretty_helpers:
    
    def __init__(self, text):
        self.newline = f'\n'
        self.green = '\033[92m'
        self.header = '\033[95m'
        self.blue = '\033[94m'
        self.cyan = '\033[96m'
        self.warning = '\033[93m'
        self.error = '\033[91m'
        self.endc = '\033[0m'
        self.bold = '\033[1m'
        self.underline = '\033[4m'

        dt = datetime.now().strftime("%H:%M:%S")
        self.ts = '{} '.format(dt)

        self.status = ''
        self.success = ''
        self.fail = ''

        if text != '':
            status_format = ' [{}]'
            self.status = status_format.format(text)
            self.success = status_format.format(self.green + text + self.endc)
            self.fail = status_format.format(self.error + text + self.endc)

        self.start_line = (self.ts + text).ljust(80, '.')
        self.status_line = self.status.rjust(25, '.')
        self.info_start_line = (self.ts + text).ljust(80, ' ')
        self.info_end_line = self.status.rjust(25, ' ')
    
class pretty_print:
    def __init__(self, start='', end='', type=None, mode='new'):        
        pretty_start_obj = pretty_helpers(start)
        pretty_end_obj = pretty_helpers(end)

        start_line = pretty_start_obj.start_line
        status_line = pretty_end_obj.status_line
        success_status = pretty_end_obj.success
        fail_status = pretty_end_obj.fail
        plain_status = pretty_end_obj.status

        info_start_line = pretty_start_obj.info_start_line
        info_end_line = pretty_end_obj.info_end_line

        if type == 'info':
            full_line = info_start_line + info_end_line
        else: 
            full_line = start_line + status_line
            if type == 'success':
                full_line = full_line.replace(plain_status, success_status)
            elif type == 'fail':
                full_line = full_line.replace(plain_status, fail_status)

        if mode == 'append':
            if start != '':
                return print(start_line, end='')
            else:
                if type == 'success':
                    return print(status_line.replace(plain_status, success_status))
                if type == 'fail':
                    return print(status_line.replace(plain_status, fail_status))
                else:
                    return print(plain_status)
        elif mode == 'overwrite':
            return print(full_line, end='\r')
        else:
            return print(full_line)