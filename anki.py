#!/usr/bin/python3

from bigquery import bq_connector
from commandparser import command_parser
from dbtrunner import dbt_runner
from ankirunner import anki_check, anki_create, anki_format


def main():
    connector = bq_connector()
    commands = command_parser().get_args()

    if commands["refresh"] == True:
        dbt_runner()

    print('\n:: Retrieving 日本語 cards from BigQuery')
    vocab_results = connector.get_results(table='dbt_cberger.anki_flashcards')

    print('\n:: Retrieving 漢字 cards from BigQuery')
    kanji_results = connector.get_results(table='dbt_cberger.kanji_writing_practice_cards')

    decks_response = anki_check.get_unsynced_decks([vocab_results, kanji_results])
    cards_response = anki_check.get_unsynced_cards(vocab_results, commands["all"])
    kanji_response = anki_check.get_unsynced_cards(kanji_results, commands["all"])

    print('\nSync status: Decks   | New: {} Synced: {}'.format(
        len(decks_response["new_decks"]),
        len(decks_response["synced_decks"])
    ))
    print('Sync staus: JP Vocab | New: {} Changed: {} Synced: {}'.format(
        len(cards_response["new_card_meta"]),
        len(cards_response["changed_card_meta"]),
        len(cards_response["synced_card_meta"])
    ))
    print('Sync staus: Kanji    | New: {} Changed: {} Synced: {}'.format(
        len(kanji_response["new_card_meta"]),
        len(kanji_response["changed_card_meta"]),
        len(kanji_response["synced_card_meta"])
    ))

    print(kanji_results)

    if decks_response["should_update"] == False \
      and cards_response["should_add"] == False \
      and cards_response["should_update"] == False \
      and kanji_response["should_add"] == False \
      and kanji_response["should_update"] == False:
        print('\nNothing to do. All caught up! \ ^-^ /\n')
    else:
        print('\nStarting upload.')
        new_decks = decks_response["new_decks"]
        new_vocab_meta = cards_response["new_card_meta"]
        changed_vocab_meta = cards_response["changed_card_meta"]
        new_kanji_meta = kanji_response["new_card_meta"]
        changed_kanji_meta = kanji_response["changed_card_meta"]
        
        new_vocab_cards = anki_format.format_cards(new_vocab_meta, vocab_results)
        new_kanji_cards = anki_format.format_cards(new_kanji_meta, kanji_results, type='kanji')

        changed_vocab_cards = anki_format.format_cards(changed_vocab_meta, vocab_results)
        changed_kanji_cards = anki_format.format_cards(changed_kanji_meta, kanji_results, type='kanji')

        new_cards = new_vocab_cards + new_kanji_cards
        changed_cards = changed_vocab_cards + changed_kanji_cards

        if len(new_decks) > 0:
            anki_create.create_decks(new_decks)
        
        if len(new_cards) > 0:
            anki_create.upload_cards(new_cards, mode='add')
        
        if len(changed_cards) > 0:
            anki_create.upload_cards(changed_cards)

        print('\nDone.\n')

if __name__ == '__main__':
    main()