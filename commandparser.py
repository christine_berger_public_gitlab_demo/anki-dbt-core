import argparse
from argparse import RawTextHelpFormatter

class command_parser:
    def __init__(self):
        self.init_parser()

    def init_parser(self):
        self.parser = argparse.ArgumentParser(description=(
            f"# -------------------------------------------------------------- #\n"
            f"#  Note:                                                         #\n"
            f"#    Anki flashcards are populated using data in your warehouse. #\n"
            f"# -------------------------------------------------------------- #"
        ), formatter_class=RawTextHelpFormatter)

        self.init_subparsers()
    
    def init_subparsers(self):
        subparsers = self.parser.add_subparsers(
            title='Required command (use build -h for more info)'
        )
        self.build_subparser = subparsers.add_parser('build', help="Rebuilds Anki flashcards with data in the warehouse")
        self.add_subparser_commands()

    def add_subparser_commands(self):
        build_group = self.build_subparser.add_argument_group('build')
        build_group.add_argument(
            '-r', '--refresh',
            action='store_true',
            help='runs dbt and performs an anki build'
        )
        build_group.add_argument(
            '-a', '--all',
            action='store_true',
            help='updates all current Anki cards vs. only updating changed Anki cards.'
        )
        
    def get_args(self):
        return vars(self.parser.parse_args())