import sys
import os
from outputs import pretty_print

class dbt_runner():
    def __init__(self):
        dbt_command = 'dbt build'

        pretty_print(start='Running dbt models', end='START')
        response = os.system(dbt_command)
        
        if response != 0:
            pretty_print(start='See above response from dbt.', end='FAIL', type='fail')
            sys.exit(response)