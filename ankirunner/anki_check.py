import numpy 
from ankirunner import anki_utils
from outputs import pretty_print


def get_unsynced_decks(result_arrays=[]):

    current_database_decks = []
    new_decks = []
    synced_decks = []
    response = False

    for results in result_arrays:
        current_anki_decks = anki_utils.invoke('deckNames')

        for record in results:
            current_database_decks.append(record["deck"])

    new_decks = numpy.setdiff1d(
        list(set(current_database_decks)), 
        current_anki_decks
    )

    synced_decks = numpy.setdiff1d(
        list(set(current_anki_decks)), 
        current_database_decks
    )

    if len(new_decks) > 0:
        response = True

    return {
        "new_decks": new_decks,
        "synced_decks": synced_decks,
        "should_update": response
    }

def get_unsynced_cards(results, update_all=False):

    synced_card_list = []
    changed_card_list = []
    new_card_list = []
    current_bigquery_card_meta = []
    current_anki_card_meta = []
    current_anki_card_ids = []

    current_anki_cards = anki_utils.invoke(
        'notesInfo', 
        notes=anki_utils.invoke('findNotes', query='card_id:*')
    )

    for result in results:
        current_bigquery_card_meta.append(get_entry_meta(result))
    
    for result in current_anki_cards:
        card_meta = get_entry_meta(result, type='anki')
        current_anki_card_meta.append(card_meta)
        current_anki_card_ids.append(card_meta["card_id"])

    for card_meta in current_bigquery_card_meta:

        if card_meta in current_anki_card_meta and update_all:
            card_meta.update({"change": "update"})
            changed_card_list.append(card_meta)

        elif card_meta in current_anki_card_meta:
            synced_card_list.append(card_meta)

        else:
            if card_meta["card_id"] in current_anki_card_ids:
                card_meta.update({"change": "update"})
                changed_card_list.append(card_meta)
            else:
                card_meta.update({"change": "new"})
                new_card_list.append(card_meta)

    return({
        "synced_card_meta": synced_card_list,
        "changed_card_meta": changed_card_list,
        "new_card_meta": new_card_list,
        "should_add": True if len(new_card_list) > 0 else False,
        "should_update": True if len(changed_card_list) > 0 else False
    })

def get_entry_meta(record, type='bq', update_all="none"):

    if type == 'bq':
        card_id = record["card_id"]
        hash = record["card_hash"]
    elif type == 'anki':
        card_id = record["fields"]["card_id"]["value"]
        hash = record["fields"]["card_hash"]["value"]

    return set_entry_meta(
        card_id=card_id,
        hash=hash,
        update_all=update_all
    )

def set_entry_meta(card_id, hash, update_all="none"):

    return {
        "card_id": card_id,
        "hash": hash,
        "change": "update" if update_all else "none"
    }