from outputs import pretty_print
from ankirunner import anki_utils, anki_format

def create_decks(deck_list=[]):

    num_decks = len(deck_list)

    if num_decks > 0:
        for deck in deck_list:
            try:
                pretty_print(start='Creating new deck \'' + deck + '\'', mode='append')
                anki_utils.invoke('createDeck', deck=deck)
                pretty_print(end='DONE', type='success', mode='append')
            except Exception as e:
                pretty_print(end='FAIL', type='fail', mode='append')
                return e

def upload_cards(cards_list=[], mode='update'):

    if len(cards_list) > 0:
        for card in cards_list:
            card_front = card["fields"]["front"]

            if mode == 'update':
                update_msg = 'Updating existing card \'{}\''.format(card_front)
                query_string = 'card_id:'+ str(card["fields"]["card_id"])
                anki_note_id = anki_utils.invoke('findNotes', query=query_string)

                pretty_print(start=update_msg, mode='append')
                try:
                    card.update({"id": anki_note_id[0]})
                    anki_utils.invoke('updateNoteFields', note=card)
                    pretty_print(end='DONE', mode='append', type='success')
                except Exception as e:
                    pretty_print(end='FAIL', mode='append', type='fail')
                    return e               
            elif mode == 'add':
                create_msg = 'Creating new card \'{}\''.format(card_front)

                pretty_print(start=create_msg, mode='append')
                try:
                    anki_utils.invoke('addNote', note=card)
                    pretty_print(end='DONE', mode='append', type='success')
                except Exception as e:
                    pretty_print(end='FAIL', mode='append', type='fail')
                    return e