def format_cards(card_meta_list, query_results_json, type='basic'):
    meta_id_list = []
    formatted_cards_list = []

    for row in card_meta_list:
        meta_id_list.append(row["card_id"])

    for row in query_results_json:
        if row["card_id"] in meta_id_list:
            formatted_cards_list.append(create_anki_card(row, type).format)

    return formatted_cards_list

class create_anki_card:

    def __init__(self, row, type='basic'):
        self.type=type
        self.deck_name=row["deck"]
        self.card_id=row["card_id"]
        self.card_hash=row["card_hash"]
        self.tag=row["tag"]
        self.allow_duplicates=False
        self.duplicate_scope='noteType'

        if type == 'basic':
            self.model_name='Basic'
            self.front=row["front"]
            self.back=row["back"]
            self.front_clarification='' if row["front_clarification"] is None else row["front_clarification"] 
            self.grammar_types='' if row["grammar_types"] is None else row["grammar_types"] 
            self.clarification='' if row["clarification"] is None else row["clarification"]
            self.secondary_form='' if row["secondary_form"] is None else row["secondary_form"]
            self.kanji_meanings='' if row["kanji_meanings"] is None else row["kanji_meanings"]

            self.format = self.new_card({
                "front_clarification": self.front_clarification,
                "grammar_type": self.grammar_types,
                "clarification": self.clarification,
                "secondary_form": self.secondary_form,
                "kanji_meanings": self.kanji_meanings
            })
        elif type == 'kanji':
            self.model_name='Kanji Basic'
            self.front=row["front"]
            self.back=row["back"]
            self.components='' if row["components"] is None else row["components"]
            self.mnemonics='' if row["mnemonics"] is None else row["mnemonics"]
            self.format = self.new_card({
                "components": self.components,
                "mnemonics": self.mnemonics
            })
    
    def new_card(self, fields):
        new_card = {
            "deckName": self.deck_name,
            "modelName": self.model_name,
            "fields": {
                "card_id": self.card_id,
                "front": self.front,
                "back": self.back,
                "card_hash": self.card_hash
            },
            "options": {
                "allowDuplicate": self.allow_duplicates,
                "duplicateScope": self.duplicate_scope
            },
            "tags": [self.tag]
        }

        new_card["fields"].update(fields)

        return new_card