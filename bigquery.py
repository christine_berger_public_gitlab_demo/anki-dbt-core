import google.auth
import pandas_gbq
from google.cloud import bigquery

class bq_connector:
    def __init__(self):
        credentials, project = google.auth.default(
            scopes=[
                "https://www.googleapis.com/auth/cloud-platform",
                "https://www.googleapis.com/auth/drive",
                "https://www.googleapis.com/auth/bigquery",
            ]
        )

        self.credentials=credentials
        self.project=project
        self.client = bigquery.Client(project, credentials)
        
        # Pass BQ credentials to pandas_gbq
        pandas_gbq.context.credentials = self.credentials

    def get_results(self, table, limit=None):
        limit_clause = '' if limit == None else 'limit {}'.format(limit)
        query = "select * from fizzy-volatile-1.{} {}".format(table, limit_clause)
        results = pandas_gbq.read_gbq(query, project_id=self.project).to_dict('records')
        return results