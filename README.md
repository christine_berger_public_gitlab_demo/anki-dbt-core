# anki-dbt-core

This project help build and manage anki cards when the cards are produced
from tables in a warehouse. 

Additionally, this project works with dbt so that you can run your dbt models
before uploading anki cards.

Currently this project is extremely customized, so probably not useful without
a lot of hands-on customization!

### Commands
Here are the acceptable commands:  
  
`$ anki build`
Checks for new and updated cards and uploads them to anki  
  
`$ anki build -r`
Runs dbt first, then proceeds with `anki build`.  
  
`$ anki build -a` Refreshes all current anki cards and adds any new cards.  
